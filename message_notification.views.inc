<?php

/**
 * @file
 * Provides views data for message_notification.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function message_notification_views_data_alter(&$data) {
  $data['message_field_data']['created_timestamp'] = [
    'filter' => [
      'title' => t('Created timestamp'),
      'id' => 'numeric',
      'real field' => 'created',
    ],
  ];
}
