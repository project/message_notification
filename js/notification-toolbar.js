(function (Drupal, once) {
  Drupal.behaviors.notificationToolbar = {
    attach: function (context, settings) {
      once("notification-toolbar", ".toolbar-tab__notification", context).forEach(
        (element) => {
          element.addEventListener("mouseover", () => {
            const tray = element.querySelector(".toolbar-tray");
            if (tray) {
              tray.classList.add("is-active")
            }
          })
          element.addEventListener("mouseout", () => {
            const tray = element.querySelector(".toolbar-tray");
            if (tray) {
              tray.classList.remove("is-active")
            }
          })
        }
      )
    }
  };

})(Drupal, once);
