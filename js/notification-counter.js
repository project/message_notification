(function (Drupal, once) {
  Drupal.behaviors.notificationCounter = {
    attach: function (context, settings) {
      once('notification-counter', '#toolbar-item-notifications', context).forEach(
        (element) => {
          async function getData() {
            const response = await fetch('/notification/counter', {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json'
              },
            });

            return response.json();
          }

          getData().then((data) => {
            if (data.count > 0) {
              element.setAttribute('data-count', data.count > 9 ? '9+' : data.count)
            }
          });

          once('notification-counter-reset', element)
            .forEach((item) => {
            item.addEventListener('mouseover', (e) => {
              if (e.target && e.target.hasAttribute('data-count')) {
                e.target.removeAttribute('data-count');
                fetch('/notification/counter-update', {
                  method: 'GET',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                });
              }
            });
          });
        }
      )
    }
  };

})(Drupal, once);
