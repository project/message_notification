(function (Drupal, once) {
  Drupal.behaviors.timeAgo = {
    attach: function (context, settings) {
      once('time-ago', '.time-ago-wrapper', context).forEach(
        (element) => {
          const unixTimestamp = element.getAttribute('data-created');
          element.textContent = convertToTimeAgo(new Date(unixTimestamp * 1000));
        }
      )
    }
  };

  function convertToTimeAgo(val) {
    val = 0 | (Date.now() - val) / 1000;
    const length = {
      second: 60,
      minute: 60,
      hour: 24,
      day: 7,
      week: 4.35,
      month: 12,
      year: 10000
    };

    for (let unit in length) {
      let result = val % length[unit];
      if (!(val = 0 | val / length[unit])) {
        return result + ' ' + (result - 1 ? unit + 's' : unit) + ' ' + Drupal.t('ago');
      }
    }
  }

})(Drupal, once);
