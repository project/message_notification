<?php

namespace Drupal\message_notification;

use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\views\Views;

/**
 * Lazy builders for the message_notification module.
 */
class NotificationLazyBuilders implements TrustedCallbackInterface {

  /**
   * Constructs a new NotificationLazyBuilders object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(
    protected readonly RendererInterface $renderer
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['lazyNotifications'];
  }

  /**
   * The lazy_builder callback.
   *
   * @return array
   *   A renderable array of shortcut links.
   */
  public function lazyNotifications() {
    $view = Views::getView('notifications');
    if (is_object($view)) {
      $view->setDisplay('notification');
      $view->preExecute();
      $view->execute();
      return [$view->buildRenderable()];
    }

    return [];
  }

}
